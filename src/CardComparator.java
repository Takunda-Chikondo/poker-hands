import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class CardComparator implements Comparator<Card> {
    public Map<String, Integer> map;
    // hash map used to get the card_rank_value associated with the card_rank

    public CardComparator(){
        map = new HashMap<>();
        map.put("two",  2);
        map.put("three",3);
        map.put("four", 4);
        map.put("five", 5);
        map.put("six",  6);
        map.put("seven",7);
        map.put("eight",8);
        map.put("nine", 9);
        map.put("ten",  10);
        map.put("jack", 11);
        map.put("queen",12);
        map.put("king", 13);
        map.put("ace",  14);
    }

    public int compare(Card a, Card b){
        return map.get(b.get_rank()) - map.get(a.get_rank());
    }
}
