public class Card {
    private String card_rank, card_suite, card_name;

    public Card(String card_rank, String card_suite){
        this.card_rank = card_rank;
        this.card_suite = card_suite;
        this.card_name = card_rank + " of " + card_suite;
    }

    public String get_name(){
        return card_name;
    }

    public String get_suite(){
        return card_suite;
    }

    public String get_rank(){
        return card_rank;
    }
}
