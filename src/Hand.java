import java.util.*;

public class Hand implements Comparable<Hand> {
    private ArrayList<Card> hand;
    private int hand_strength;
    // assigns one of the 10 rank strengths to a hand
    private String hand_rank;
    private Map<String, Integer> suit_count, rank_count;
    private CardComparator comparator;

    public Hand(ArrayList<Card> initial_hand){
        hand = new ArrayList<>();
        hand.addAll(initial_hand);
        comparator = new CardComparator();
        // create comparator once per hand instance
        calculate_hand_rank();
    }

    public int get_number_of_cards(){ return hand.size(); }

    public int get_hand_strength(){
        return hand_strength;
    }

    public String get_hand_rank(){
        return hand_rank;
    }

    public int compareTo(Hand a){
        if(a.hand_strength < hand_strength){
            return 1;
        }
        else if (a.hand_strength > hand_strength) {
            return -1;
        }
        return 0;
    }

    private void calculate_hand_rank(){
        Map frequencies;
        String key;
        Card card;

        Collections.sort(hand, comparator);
        // sort the cards based on their rank

        if(consecutiveCount() == 5  && count_suits().containsValue(5)){
            // if consecutive count is 5 royal/straight flush

            if(hand.get(0).get_rank().equals("ace")){
                hand_rank = "royal flush";
                hand_strength = 10;
                // royal flush has highest rank
            }
            else if(hand.get(0).get_rank().equals("eight")){
                // straight flush
                hand_rank = "straight flush";
                hand_strength = 9;
            }
        }
        else if(get_rank_frequencies().containsValue(4)){
            // if the are 4 cards of the same rank then 4 out 5 then 4 of a kind
            hand_rank = "four of a kind";
            hand_strength = 8;
        }

        else if((frequencies = get_rank_frequencies()).containsValue(3) && frequencies.containsValue(2)){
            // if 3 and 2 of the same rank full house
            hand_rank = "full house";
            hand_strength = 7;
        }

        else if(consecutiveCount() == 0 && count_suits().containsValue(5)){
            hand_rank = "flush";
            hand_strength = 6;
        }

        else if(consecutiveCount()==5 && count_suits().size() >= 2){
            hand_rank = "straight";
            hand_strength = 5;
        }

        else if((frequencies = get_rank_frequencies()).containsValue(3) && frequencies.size() == 3){

            for(int i= 0; i < hand.size(); i++){
                card = hand.get(i);
                key = card.get_rank();

                if(rank_count.get(key) == 3){
                    // check if there are three cards of this rank
                    count_suits().remove(card.get_suite());
                }
            }
            if(get_suit_count_frequency(1) == 2){
                // if they are 3 different suits
                hand_rank = "3 of a kind";
                hand_strength = 4;
            }
        }

        else if((frequencies = get_rank_frequencies()).containsValue(2) && frequencies.size() == 4){
            for(int i= 0; i < hand.size(); i++){
                card = hand.get(i);
                key = card.get_rank();

                if(rank_count.get(key) == 2){
                    count_suits().remove(card.get_suite());
                }
            }
            if(get_suit_count_frequency(1) == 3){
                // if they are 3 different suits
                hand_rank = "1 pair";
                hand_strength = 2;
            }
        }

        else if((frequencies = get_rank_frequencies()).containsValue(2)){
            for(int i=0; i<hand.size(); i++){
                key = hand.get(i).get_rank();

                if(frequencies.containsKey(key) && rank_count.get(key) == 2){
                    frequencies.remove(key);
                    break;
                }
            }
            if(frequencies.containsValue(2)){
                hand_rank = "2 pair";
                hand_strength = 3;
            }
        }

        else{
            // if none of the above then iit is a high card
            hand_rank = "high card";
            hand_strength = 1;
        }
    }

    private int consecutiveCount(){
        //count the number of consecutively ranked cards
        int count = 1;
        Card card_a, card_b;

        for (int i = 0; i < 4; i++){

            card_a = hand.get(i);
            card_b = hand.get(i+1);

            int card_a_value  = comparator.map.get(card_a.get_rank());
            int card_b_value  = comparator.map.get(card_b.get_rank());

            int difference = card_a_value - card_b_value;

            if(difference == 1){
                count ++;
            }
            else{
                count = 0;
            }
        }
        return count;
    }

    private Map count_suits(){
        // gets the frequency of each suit in the hand

        suit_count = new HashMap<>();

        Card card;
        for(int i= 0; i<hand.size(); i++){
            card = hand.get(i);
            if(suit_count.get(card.get_suite()) == null){
                // if the suite is not in the dictionary add it
                suit_count.put(card.get_suite(), 1);
            }
            else{
                int temp = suit_count.get(card.get_suite());
                // if suite already in dictionary increment count
                suit_count.put(card.get_suite(), temp+1);
            }
        }
        return suit_count;
    }

    private Map get_rank_frequencies(){
        rank_count = new HashMap<>();

        Card card;
        for(int i=0; i<hand.size(); i++){
            card = hand.get(i);
            if(rank_count.get(card.get_rank()) == null){
                rank_count.put(card.get_rank(), 1);
            }
            else{
                int temp = rank_count.get(card.get_rank());
                rank_count.put(card.get_rank(), temp+1);
            }
        }
        return rank_count;
    }

    private int get_suit_count_frequency(int num){
        // get the number of ranks with a certain frequency
        int count  = Collections.frequency(suit_count.values(), num);
        return count;
    }
}
