import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class TestCardGame {

    @Test
    public void get_name() {
        Card card = new Card("two", "clubs");
        String card_name = card.get_name();
        String expected_card_name = "two of clubs";
        assertEquals(expected_card_name, card_name);
    }

    @Test
    public void test_deck() {
        Deck deck = new Deck();
        assertEquals(52, deck.get_number_of_cards());
    }

    @Test
    public void pick() {
        Deck deck = new Deck();
        assertEquals(52, deck.get_number_of_cards());

        deck.pick(1);
        assertEquals(51, deck.get_number_of_cards());
    }

    @Test
    public void pick_max() {
        // test picking of 51 cards the maximum allowed
        Deck deck = new Deck();
        assertEquals(52, deck.get_number_of_cards());

        deck.pick(51);
        assertEquals(1, deck.get_number_of_cards());
    }


    @Test
    public void test_pick_hand() {
        Deck deck = new Deck();
        assertEquals(52, deck.get_number_of_cards());

        Hand hand = new Hand(deck.pick(5));
        assertEquals(47, deck.get_number_of_cards());

        assertEquals(5, hand.get_number_of_cards());
    }

    @Test
    public void test_royal_flush(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
            new Card("ace", "hearts"),
            new Card("king","hearts"),
            new Card("queen","hearts"),
            new Card("jack","hearts"),
            new Card("ten", "hearts")
        ));

        Hand hand = new Hand(cards);
        String hand_rank = hand.get_hand_rank();

        assertEquals("royal flush", hand_rank);
    }

    @Test
    public void test_straight_flush(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("five", "spades"),
                new Card("seven","spades"),
                new Card("six","spades"),
                new Card("four","spades"),
                new Card("eight", "spades")
        ));

        Hand hand = new Hand(cards);
        String hand_rank = hand.get_hand_rank();

        assertEquals("straight flush", hand_rank);
    }

    @Test
    public void test_four_of_a_kind(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("five", "spades"),
                new Card("five","hearts"),
                new Card("five","clubs"),
                new Card("five","diamonds"),
                new Card("two", "spades")
        ));

        Hand hand = new Hand(cards);
        String hand_rank = hand.get_hand_rank();

        assertEquals("four of a kind", hand_rank);
    }

    @Test
    public void test_full_house(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("king", "spades"),
                new Card("king","hearts"),
                new Card("king","clubs"),
                new Card("five","diamonds"),
                new Card("five", "spades")
        ));

        Hand hand = new Hand(cards);
        String hand_rank = hand.get_hand_rank();

        assertEquals("full house", hand_rank);
    }

    @Test
    public void test_flush(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("king", "spades"),
                new Card("jack","spades"),
                new Card("nine","spades"),
                new Card("seven","spades"),
                new Card("three", "spades")
        ));

        Hand hand = new Hand(cards);
        String hand_rank = hand.get_hand_rank();

        assertEquals("flush" ,hand_rank);
    }

    @Test
    public void test_straight(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("queen", "spades"),
                new Card("jack","diamonds"),
                new Card("ten","clubs"),
                new Card("nine","spades"),
                new Card("eight", "hearts")
        ));

        Hand hand = new Hand(cards);
        String hand_rank = hand.get_hand_rank();

        assertEquals("straight", hand_rank);
    }

    @Test
    public void test_three_of_a_kind(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("queen", "spades"),
                new Card("queen","hearts"),
                new Card("queen","diamonds"),
                new Card("five","spades"),
                new Card("nine", "clubs")
        ));

        Hand hand = new Hand(cards);
        String hand_rank = hand.get_hand_rank();

        assertEquals("3 of a kind", hand_rank);
    }

    @Test
    public void test_2_pair(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("king", "hearts"),
                new Card("king","spades"),
                new Card("jack","clubs"),
                new Card("jack","diamonds"),
                new Card("nine", "diamonds")
        ));

        Hand hand = new Hand(cards);
        String hand_rank = hand.get_hand_rank();

        assertEquals("2 pair", hand_rank);
    }

    @Test
    public void test_1_pair(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("ace", "clubs"),
                new Card("ace","diamonds"),
                new Card("nine","hearts"),
                new Card("six","spades"),
                new Card("four", "diamonds")
        ));

        Hand hand = new Hand(cards);
        String hand_rank = hand.get_hand_rank();

        assertEquals("1 pair" ,hand_rank);
    }

    @Test
    public void high_card(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("ace", "diamonds"),
                new Card("seven","hearts"),
                new Card("five","spades"),
                new Card("three","diamonds"),
                new Card("two", "spades")
        ));

        Hand hand = new Hand(cards);
        String hand_rank = hand.get_hand_rank();

        assertEquals("high card" ,hand_rank);
    }

    @Test
    public void test_compare_flushes(){
        // compare hands a and b
        // a.compareTo(b) returns 1 if a > b
        // cannot override operators in java hence used compare to
        // checks if straight flush is less than royal flush
        ArrayList<Card> cards;

        cards = new ArrayList<>(Arrays.asList(
                new Card("ace", "hearts"),
                new Card("king","hearts"),
                new Card("queen","hearts"),
                new Card("jack","hearts"),
                new Card("ten", "hearts")
        ));

        Hand royal_flush = new Hand(cards);

        cards = new ArrayList<>(Arrays.asList(
                new Card("five", "spades"),
                new Card("seven","spades"),
                new Card("six","spades"),
                new Card("four","spades"),
                new Card("eight", "spades")
        ));

        Hand straight_flush = new Hand(cards);

        assertEquals(-1, straight_flush.compareTo(royal_flush));
    }

    @Test
    public void test_compare_straight_flush_and_four_of_kind(){
        ArrayList<Card> cards;

        cards = new ArrayList<>(Arrays.asList(
                new Card("five", "spades"),
                new Card("seven","spades"),
                new Card("six","spades"),
                new Card("four","spades"),
                new Card("eight", "spades")
        ));

        Hand straight_flush = new Hand(cards);

        cards = new ArrayList<>(Arrays.asList(
                new Card("five", "spades"),
                new Card("five","hearts"),
                new Card("five","clubs"),
                new Card("five","diamonds"),
                new Card("two", "spades")
        ));

        Hand four_of_kind = new Hand(cards);

        assertEquals(1, straight_flush.compareTo(four_of_kind));
    }

    @Test
    public void test_compare_four_of_kind_and_full_house(){
        ArrayList<Card> cards;

        cards = new ArrayList<>(Arrays.asList(
                new Card("five", "spades"),
                new Card("five","hearts"),
                new Card("five","clubs"),
                new Card("five","diamonds"),
                new Card("two", "spades")
        ));

        Hand four_of_kind = new Hand(cards);

        cards = new ArrayList<>(Arrays.asList(
                new Card("king", "spades"),
                new Card("king","hearts"),
                new Card("king","clubs"),
                new Card("five","diamonds"),
                new Card("five", "spades")
        ));

        Hand full_house = new Hand(cards);

        assertEquals(1, four_of_kind.compareTo(full_house));
    }

    @Test
    public void test_compare_full_house_and_flush(){
        ArrayList<Card> cards;

        cards = new ArrayList<>(Arrays.asList(
                new Card("king", "spades"),
                new Card("king","hearts"),
                new Card("king","clubs"),
                new Card("five","diamonds"),
                new Card("five", "spades")
        ));

        Hand full_house = new Hand(cards);

        cards = new ArrayList<>(Arrays.asList(
                new Card("king", "spades"),
                new Card("jack","spades"),
                new Card("nine","spades"),
                new Card("seven","spades"),
                new Card("three", "spades")
        ));

        Hand flush = new Hand(cards);

        assertEquals(1, full_house.compareTo(flush));
    }

    @Test
    public void test_compare_flush_and_straight(){
        ArrayList<Card> cards;

        cards = new ArrayList<>(Arrays.asList(
                new Card("king", "spades"),
                new Card("king","hearts"),
                new Card("king","clubs"),
                new Card("five","diamonds"),
                new Card("five", "spades")
        ));

        Hand flush = new Hand(cards);

        cards = new ArrayList<>(Arrays.asList(
                new Card("king", "spades"),
                new Card("jack","spades"),
                new Card("nine","spades"),
                new Card("seven","spades"),
                new Card("three", "spades")
        ));

        Hand straight = new Hand(cards);

        assertEquals(1, flush.compareTo(straight));
    }

    @Test
    public void test_compare_straight_and_three_of_a_kind(){
        ArrayList<Card> cards;

        cards = new ArrayList<>(Arrays.asList(
                new Card("queen", "spades"),
                new Card("jack","diamonds"),
                new Card("ten","clubs"),
                new Card("nine","spades"),
                new Card("eight", "hearts")
        ));

        Hand straight = new Hand(cards);

        cards = new ArrayList<>(Arrays.asList(
                new Card("queen", "spades"),
                new Card("queen","hearts"),
                new Card("queen","diamonds"),
                new Card("five","spades"),
                new Card("nine", "clubs")
        ));

        Hand three_of_a_kind = new Hand(cards);

        assertEquals(1, straight.compareTo(three_of_a_kind));
    }

    @Test
    public void test_compare_three_of_a_kind_and_two_pair(){
        ArrayList<Card> cards;

        cards = new ArrayList<>(Arrays.asList(
                new Card("queen", "spades"),
                new Card("queen","hearts"),
                new Card("queen","diamonds"),
                new Card("five","spades"),
                new Card("nine", "clubs")
        ));

        Hand three_of_a_kind = new Hand(cards);

        cards = new ArrayList<>(Arrays.asList(
                new Card("king", "hearts"),
                new Card("king","spades"),
                new Card("jack","clubs"),
                new Card("jack","diamonds"),
                new Card("nine", "diamonds")
        ));

        Hand two_pair = new Hand(cards);

        assertEquals(1, three_of_a_kind.compareTo(two_pair));
    }

    @Test
    public void test_compare_two_pair_and_one_pair(){
        ArrayList<Card> cards;

        cards = new ArrayList<>(Arrays.asList(
                new Card("king", "hearts"),
                new Card("king","spades"),
                new Card("jack","clubs"),
                new Card("jack","diamonds"),
                new Card("nine", "diamonds")
        ));

        Hand two_pair = new Hand(cards);

        cards = new ArrayList<>(Arrays.asList(
                new Card("ace", "clubs"),
                new Card("ace","diamonds"),
                new Card("nine","hearts"),
                new Card("six","spades"),
                new Card("four", "diamonds")
        ));

        Hand one_pair = new Hand(cards);

        assertEquals(1, two_pair.compareTo(one_pair));
    }

    @Test
    public void test_compare_one_pair_and_high_card(){
        ArrayList<Card> cards;

        cards = new ArrayList<>(Arrays.asList(
                new Card("ace", "clubs"),
                new Card("ace","diamonds"),
                new Card("nine","hearts"),
                new Card("six","spades"),
                new Card("four", "diamonds")
        ));

        Hand one_pair = new Hand(cards);

        cards = new ArrayList<>(Arrays.asList(
                new Card("ace", "diamonds"),
                new Card("seven","hearts"),
                new Card("five","spades"),
                new Card("three","diamonds"),
                new Card("two", "spades")
        ));

        Hand high_card = new Hand(cards);

        assertEquals(1, one_pair.compareTo(high_card));
    }

    @Test
    public void test_compare_royal_flush_and_high_card(){
        ArrayList<Card> cards;

        cards = new ArrayList<>(Arrays.asList(
                new Card("ace", "hearts"),
                new Card("king","hearts"),
                new Card("queen","hearts"),
                new Card("jack","hearts"),
                new Card("ten", "hearts")
        ));

        Hand royal_flush = new Hand(cards);

        cards = new ArrayList<>(Arrays.asList(
                new Card("ace", "diamonds"),
                new Card("seven","hearts"),
                new Card("five","spades"),
                new Card("three","diamonds"),
                new Card("two", "spades")
        ));

        Hand high_card = new Hand(cards);

        assertEquals(1, royal_flush.compareTo(high_card));
    }

    //custom unit tests
    @Test
    public void test_royal_flush_strength(){
        // test the card strength
        // used to compare the hands
        // strength ranked from 1 to 10 with 10 being royal flush
        // one being high card lowest strength hand
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("ace", "hearts"),
                new Card("king","hearts"),
                new Card("queen","hearts"),
                new Card("jack","hearts"),
                new Card("ten", "hearts")
        ));

        Hand hand = new Hand(cards);
        int hand_strength = hand.get_hand_strength();

        assertEquals(10, hand_strength);
    }

    @Test
    public void test_straight_flush_strength(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("five", "spades"),
                new Card("seven","spades"),
                new Card("six","spades"),
                new Card("four","spades"),
                new Card("eight", "spades")
        ));

        Hand hand = new Hand(cards);
        int hand_strength = hand.get_hand_strength();

        assertEquals(9, hand_strength);
    }

    @Test
    public void test_four_of_a_kind_strength(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("five", "spades"),
                new Card("five","hearts"),
                new Card("five","clubs"),
                new Card("five","diamonds"),
                new Card("two", "spades")
        ));

        Hand hand = new Hand(cards);
        int hand_strength = hand.get_hand_strength();

        assertEquals(8, hand_strength);
    }

    @Test
    public void test_full_house_strength(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("king", "spades"),
                new Card("king","hearts"),
                new Card("king","clubs"),
                new Card("five","diamonds"),
                new Card("five", "spades")
        ));

        Hand hand = new Hand(cards);
        int hand_strength = hand.get_hand_strength();

        assertEquals(7, hand_strength);
    }

    @Test
    public void test_flush_strength(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("king", "spades"),
                new Card("jack","spades"),
                new Card("nine","spades"),
                new Card("seven","spades"),
                new Card("three", "spades")
        ));

        Hand hand = new Hand(cards);
        int hand_strength = hand.get_hand_strength();

        assertEquals(6, hand_strength);
    }

    @Test
    public void test_straight_strength(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("queen", "spades"),
                new Card("jack","diamonds"),
                new Card("ten","clubs"),
                new Card("nine","spades"),
                new Card("eight", "hearts")
        ));

        Hand hand = new Hand(cards);
        int hand_strength = hand.get_hand_strength();

        assertEquals(5, hand_strength);
    }

    @Test
    public void test_three_of_a_kind_strength(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("queen", "spades"),
                new Card("queen","hearts"),
                new Card("queen","diamonds"),
                new Card("five","spades"),
                new Card("nine", "clubs")
        ));

        Hand hand = new Hand(cards);
        int hand_strength = hand.get_hand_strength();

        assertEquals(4, hand_strength);
    }

    @Test
    public void test_2_pair_strength(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("king", "hearts"),
                new Card("king","spades"),
                new Card("jack","clubs"),
                new Card("jack","diamonds"),
                new Card("nine", "diamonds")
        ));

        Hand hand = new Hand(cards);
        int hand_strength = hand.get_hand_strength();

        assertEquals(3, hand_strength);
    }

    @Test
    public void test_1_pair_strength(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("ace", "clubs"),
                new Card("ace","diamonds"),
                new Card("nine","hearts"),
                new Card("six","spades"),
                new Card("four", "diamonds")
        ));

        Hand hand = new Hand(cards);
        int hand_strength = hand.get_hand_strength();

        assertEquals(2, hand_strength);
    }

    @Test
    public void test_high_card_strength(){
        ArrayList<Card> cards = new ArrayList<>(Arrays.asList(
                new Card("ace", "diamonds"),
                new Card("seven","hearts"),
                new Card("five","spades"),
                new Card("three","diamonds"),
                new Card("two", "spades")
        ));

        Hand hand = new Hand(cards);
        int hand_strength = hand.get_hand_strength();

        assertEquals(1, hand_strength);
    }
}