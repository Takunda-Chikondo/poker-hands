import java.util.ArrayList;
import java.util.Collections;

public class Deck {
    private ArrayList<Card> deck;
    private String [] card_ranks, card_suites;

    public Deck(){
        deck = new ArrayList<>();
        //create array list of cards
        card_ranks = new String []
        {"two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "jack", "queen", "king", "ace"};

        card_suites = new String []
        {"clubs", "diamonds", "hearts", "spades"};
        populate_deck();
    }

    public ArrayList<Card> pick(int num_cards){
        Card card;
        ArrayList<Card> picked_cards = new ArrayList<>();
        // ensures the player can only pick less than 52 cards
        if (num_cards > 51){
            num_cards = 51;
        }

        for(int i= 0; i<num_cards; i++){
            card = deck.remove(0);
            // remove the item from the top if the deck
            picked_cards.add(card);
        }
        return picked_cards;
    }

    public int get_number_of_cards(){
        return deck.size();
    }

    public void populate_deck(){
        // creates the set of cards and adds them to the desk
        for (int i=0; i<4; i++){
            for (int j = 0; j<13; j++){
                Card card = new Card(card_ranks[j], card_suites[i]);
                deck.add(card);
            }
        }
        Collections.shuffle(deck);
        // shuffle the cards in the deck
    }
}
